package com.recr.natife.adapter;

import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import com.recr.natife.R;

import androidx.annotation.RequiresApi;

import com.recr.natife.adapter.holder.MyViewHolder;
import com.recr.natife.model.ModelItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class CustomAdapter extends BaseAdapter {

    Context c;
    Activity activity;
    ArrayList<ModelItem> subjects;
    LayoutInflater inflater;
    ModelItem teachersSetGet;
    Map<Integer, View> myviews;
    SharedPreferences sp;


    public CustomAdapter(Context c, ArrayList<ModelItem> subjects, Map<Integer, View> myviews){
        this.c = c;
        this.subjects = subjects;
        this.myviews = myviews;
    }
    @Override
    public int getCount() {
        return subjects.size();
          }

    @Override
    public Object getItem(int position) {
        return subjects.get(position);
    }
//1433950405
    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (!myviews.containsKey(position)) {
            inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.model, parent, false);
            final MyViewHolder holderSub = new MyViewHolder(convertView);

            teachersSetGet = subjects.get(position);
            holderSub.gifView1.loadAsset(teachersSetGet.getUrl1());
            holderSub.gifView2.loadAsset(teachersSetGet.getUrl2());
            holderSub.gifView3.loadAsset(teachersSetGet.getUrl3());
            holderSub.gifView4.loadAsset(teachersSetGet.getUrl4());


            final View finalConvertView = convertView;


        } else {
        convertView = myviews.get(position);
    }
        return convertView;
    }




}
