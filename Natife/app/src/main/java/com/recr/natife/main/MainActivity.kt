package com.recr.natife.main

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.giphy.sdk.analytics.GiphyPingbacks.context
import com.giphy.sdk.ui.Giphy
import com.recr.natife.R
import com.recr.natife.adapter.CustomAdapter
import com.recr.natife.model.ModelItem
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import java.util.*


@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    internal var asyncQuery: CallAPI? = null
    var adapter: CustomAdapter? = null
    var arraySetGets: ArrayList<ModelItem> = ArrayList<ModelItem>()


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Giphy.configure(this, "lzAGcpA7K8H0VktWirX6qHiXEwO8ekco")
        setContentView(R.layout.activity_main)
        val listView = findViewById<ListView>(R.id.ListView)
        val search = findViewById<EditText>(R.id.textSearch)
        val meviews: Map<Int, View> = HashMap()
        adapter = CustomAdapter(this, arraySetGets, meviews)
        arraySetGets.clear()
        asyncQuery = CallAPI()
        asyncQuery!!.initArray(arraySetGets, listView, adapter!!, search.text.toString())
        asyncQuery!!.execute()
        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                arraySetGets.clear()
                asyncQuery = CallAPI()
                asyncQuery!!.initArray(arraySetGets, listView, adapter!!, search.text.toString())
                asyncQuery!!.execute()
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                // TODO Auto-generated method stub
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                // TODO Auto-generated method stub
            }
        })
        if (!internet_connection()){
            search.setText("Disconnect")
        }

    }

    fun internet_connection(): Boolean {
        //Check if connected to internet, output accordingly
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting
    }

    internal class CallAPI : AsyncTask<String?, String?, String>() {

        val Api_Key: String = "lzAGcpA7K8H0VktWirX6qHiXEwO8ekco"
        var limit = 1945
        var type = "cats"
        var index = 0
        var arraySetGets: ArrayList<ModelItem>? = null
        @SuppressLint("StaticFieldLeak")
        var list: ListView? = null
        var adapterList: CustomAdapter? = null


        fun internet_connection(): Boolean {
            //Check if connected to internet, output accordingly
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting
        }

        fun getJsonFromAssets(context: Context, fileName: String?): String? {
            val jsonString: String
            jsonString = try {
                val `is`: InputStream = context.getAssets().open(fileName.toString())
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                String(buffer, Charset.forName("UTF-8"))
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return jsonString
        }

        override fun doInBackground(vararg params: String?): String? {
            val urlString = "https://api.giphy.com/v1/gifs/search?api_key="+Api_Key+"&limit="+limit+"&q="+type // URL to call
            if (internet_connection()) {
                return sendGetRequest(urlString)
            }else{
                val jsonFileString: String = getJsonFromAssets(context, "localecontent.json").toString()
                return jsonFileString
            }
        }
           override fun onPostExecute(result: String?) {
               val obj = JSONObject(result)
               val userArray = obj.getJSONArray("data")
               var setterGetter: ModelItem? = null
               for (i in 0 until userArray.length()) {
                   val userDetail = userArray.getJSONObject(i)
                   println(userDetail.getString("images"))
                   val images = JSONObject(userDetail.getString("images").toString())
                   val originals = images.get("original").toString()
                   println(originals)
                   val original = JSONObject(originals)
                   val urls = original.get("url")
                   println(urls)
                   if (index==0) {
                       setterGetter = ModelItem()
                       setterGetter.url1 = urls.toString()
                       index = 1
                   }else if (index==1){
                       setterGetter!!.url2 = urls as String?
                       index = 2

                   }else if (index == 2){
                       setterGetter!!.url3 = urls as String?
                       index = 3

                   }else if (index == 3){
                       setterGetter!!.url4 = urls as String?
                       arraySetGets!!.add(setterGetter)
                       index = 0
                   }


               }
               setAdapter(list!!, adapterList!!)
            super.onPostExecute(result)
        }
        fun setAdapter(listView: ListView, adapter: CustomAdapter){
            listView.adapter = adapter

        }
        fun initArray(
            array: ArrayList<ModelItem>?,
            listView: ListView,
            adapter: CustomAdapter,
            search: String
        ){
            arraySetGets = array
            list = listView
            adapterList = adapter
            type = search
            }

    fun sendGetRequest(url: String): String? {

            val mURL = URL(url)

            with(mURL.openConnection() as HttpURLConnection) {
                requestMethod = "GET"

                println("URL : $url")
                println("Response Code : $responseCode")
                BufferedReader(InputStreamReader(inputStream)).use {
                    val response = StringBuffer()

                    var inputLine = it.readLine()
                    while (inputLine != null) {
                        response.append(inputLine)
                        inputLine = it.readLine()
                    }
                    it.close()
                    println("Response : $response")
                    return response.toString()
                }
            }
        }

    }

}