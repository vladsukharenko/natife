package com.recr.natife.adapter.holder;

import android.view.View;

import com.giphy.sdk.ui.views.GifView;
import com.recr.natife.R;


public class MyViewHolder {

    public GifView gifView1;
    public GifView gifView2;
    public GifView gifView3;
    public GifView gifView4;


    public MyViewHolder(View v ){
       gifView1 = v.findViewById(R.id.gifView1);
       gifView2 = v.findViewById(R.id.gifView2);
       gifView3 = v.findViewById(R.id.gifView3);
       gifView4 = v.findViewById(R.id.gifView4);


    }

}
